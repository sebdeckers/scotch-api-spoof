const { join } = require('path')

const key = join(__dirname, 'key.pem')
const cert = join(__dirname, 'cert.pem')

module.exports.key = key
module.exports.cert = cert

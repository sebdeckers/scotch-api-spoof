# scotch-api-spoof 🥃

Scotch uses a Unique Device ID to associate the installation of MacOS with a trial period. While it is possible to reset the UDID by reinstalling MacOS, buying a new Apple computer, or running in a Virtual Machine, these solutions are inconvenient at best.

In the past one could simply block the update checks, using a firewall like Little Snitch or DNS misdirection in `/etc/hosts`, and the trial would continue to operate.

Recent changes in the Scotch app make the trial lock-up if the license check fails. However the trial continues to work while the check is in progress.

This can be taken advantage of by spoofing the Scotch API to accept incoming connections but never complete the response, trickling inert whitespace forever.

This seems to work for many hours, though eventually Scotch reports that the check failed and requires a restart.

## Step 0: Get Scotch

Download and install the Scotch trial.

## Step 1: Install `scotch-api-spoof`

```
git clone git@gitlab.com:sebdeckers/scotch-api-spoof.git
cd scotch-api-spoof
npm i
```

Hint: One step is omitted here for your inconvenience. It involves updating a fairly obvous *typo* all over the code.

## Step 2: Reset Scotch Demo License

```
rm ~/Library/Application\ Support/com.bohemiancoding.scotch3/.license
```

## Step 3: Hi-jack DNS

File: `/etc/hosts`

```
::1 api.scotchapp.com
::1 backend.bohemiancoding.com
::1 www.scotchapp.com
::1 download.scotchapp.com
::1 scotchapp.com
```

## Step 4: Generate Self-Signed Certificate

Creates `key.pem` and `cert.pem` in this directory.

```
node setup.js
```

Note: Might require manually assigning trust using Keychain Access. See: https://gitlab.com/sebdeckers/howto-https-localhost-trusted-ssl-certificate

## Step 5: Run the Mock API

Runs a webserver that accepts any connection, logs incoming requests, and forever slow-drips whitespace to the client.

```
sudo node server.js
```

Note: This requires `sudo` because it listens on port `443` (HTTPS).

## Step 6: Launch Scotch

Make beautiful things.

## Expected Output

Launching Scotch should result in a few network calls being logged.

```
GET /1/scotch/trial/?udid=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&application=scotch3&version=48.1&device_name=YYYYYYYYYYYYYYYYYYYYYYYYYYYY
{ host: 'api.scotchapp.com',
  accept: '*/*',
  cookie: '__cfduid=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  'user-agent': 'Scotch/47250 CFNetwork/893.13.1 Darwin/17.3.0 (x86_64)',
  'accept-language': 'ZZZZZ',
  'accept-encoding': 'gzip, deflate',
  connection: 'keep-alive' }
GET /newstrigger/news.php?buildnr=47250&version=48.1&bundleid=com.bohemiancoding.scotch3&variant=NONAPPSTORE
{ host: 'backend.bohemiancoding.com',
  accept: '*/*',
  'accept-language': 'ZZZZZ',
  connection: 'keep-alive',
  'accept-encoding': 'gzip, deflate',
  'user-agent': 'Scotch/47250 CFNetwork/893.13.1 Darwin/17.3.0 (x86_64)' }
GET /analytics/analytics.html
{ host: 'www.scotchapp.com',
  accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
  cookie: '__cfduid=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko)',
  'accept-language': 'ZZZZZ',
  'accept-encoding': 'gzip, deflate',
  connection: 'keep-alive' }
```

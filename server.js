const { createServer } = require('https')
const { readFileSync } = require('fs')
const { key, cert } = require('./configuration')

async function main () {
  const options = {
    key: readFileSync(key),
    cert: readFileSync(cert)
  }

  createServer(options, (request, response) => {
    console.log(request.method, request.url)
    console.log(request.headers)
    setInterval(() => response.write(' '), 5000)
  }).listen(443, (error) => {
    if (error) throw error
    console.log('Server listening...')
  })
}

main()

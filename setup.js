const { key, cert } = require('./configuration')
const { setupTLS } = require('./setupTLS')

async function setup () {
  await setupTLS(key, cert)
}

setup()
